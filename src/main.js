// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueHead from 'vue-head'
import VueGtm from 'vue-gtm'
import VeeValidate from 'vee-validate'

Vue.config.productionTip = false
Vue.use(VueHead)
Vue.use(VeeValidate, {
  classes: true,
  classNames: {
    invalid: 'has-error'
  }
})

Vue.use(VueGtm, {
  id: 'GTM-K7BSQHC',
  enabled: process.env.NODE_ENV === 'production',
  debug: false,
  vueRouter: router
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
