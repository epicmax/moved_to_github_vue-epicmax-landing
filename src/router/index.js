import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Layout from '@/components/Layout'
import Works from '@/components/Works'
import Contact from '@/components/contact/Contact'
import ThankYou from '@/components/contact/ThankYou'
import About from '@/components/About'
import NewsletterSubscription from '@/components/NewsletterSubscription'
import CasePage from '@/components/CasePage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Layout,
      children: [
        {
          name: 'Home',
          path: '',
          component: Home
        },
        {
          name: 'Works',
          path: 'works',
          component: Works
        },
        {
          name: 'About',
          path: 'about',
          component: About
        },
        {
          name: 'Contact',
          path: 'contact',
          component: Contact
        },
        {
          name: 'ThankYou',
          path: 'thankYou',
          component: ThankYou,
          beforeEnter: (to, from, next) => to.params.isFormSubmitted ? next() : next('/')
        },
        {
          name: 'Newsletter',
          path: 'newsletter',
          component: NewsletterSubscription
        },
        {
          name: 'CasePage',
          path: 'case-page',
          component: CasePage
        },
        {
          path: 'vuestic',
          redirect: 'works'
        },
        {
          path: '*',
          redirect: {
            name: 'Home'
          }
        }
      ]
    }
  ]
})
